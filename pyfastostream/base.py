from abc import ABC, abstractmethod

STREAM_BEST_QUALITY = "best"
STREAM_WORST_QUALITY = "worst"
STREAM_SYNONYMS = [STREAM_BEST_QUALITY, STREAM_WORST_QUALITY]

PREFER_AUDIO = 0
PREFER_VIDEO = 1
PREFER_BOTH = 2

DOWNLOAD_CHUNK_SIZE = 8192
RETRY_OPEN_STREAM = 3


class IBaseStreamLink(ABC):
    @abstractmethod
    def resolve_url(self, url: str, stream_name: str, prefer: int) -> str:
        pass
