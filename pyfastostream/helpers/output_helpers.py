import sys
import requests

from pathlib import Path
from contextlib import closing
from typing import Any, Iterator, Tuple
from functools import partial
from itertools import chain

from streamlink.stream.stream import Stream, StreamIO

from pyfastostream.base import DOWNLOAD_CHUNK_SIZE, RETRY_OPEN_STREAM
from pyfastostream.output import FileOutput


def create_output(out: str) -> FileOutput:
    """
    Create file output
    """
    return FileOutput(Path(out))


def open_stream(stream: Stream) -> Tuple[StreamIO, Any]:
    """
    Open a stream and reads 8192 bytes from it
    """
    stream_fd = stream.open()

    try:
        prebuffer = stream_fd.read(DOWNLOAD_CHUNK_SIZE)
    except Exception:
        stream_fd.close()
        raise Exception

    if not prebuffer:
        stream_fd.close()
        raise Exception

    return stream_fd, prebuffer


def save_video_streamlink(stream: Stream, out: str) -> None:
    """
    Streamlink version
    """
    output = create_output(out)

    success_open = False
    for _ in range(RETRY_OPEN_STREAM):
        try:
            stream_fd, prebuffer = open_stream(stream)
            success_open = True
            break
        except Exception:
            pass

    if not success_open:
        sys.exit(1)

    try:
        output.open()
    except OSError:
        sys.exit(1)

    with closing(output):
        read_stream(output, stream_fd, prebuffer)


def read_stream(output: FileOutput, stream, prebuffer):
    """
    Read stream data and write it to the output
    """
    stream_iterator: Iterator = chain(
        [prebuffer], iter(partial(stream.read, DOWNLOAD_CHUNK_SIZE), b"")
    )
    try:
        for data in stream_iterator:
            try:
                output.write(data)
            except OSError:
                sys.exit(1)
    except OSError:
        sys.exit(1)
    finally:
        stream.close()


def save_video_youtube(url: str, out: str) -> None:
    """
    Youtube version
    """
    output = create_output(out)

    success_open = False
    for _ in range(RETRY_OPEN_STREAM):
        try:
            stream = requests.get(url, stream=True)
            success_open = True
            break
        except requests.ConnectionError:
            pass

    if not success_open:
        sys.exit(1)

    try:
        output.open()
    except OSError:
        sys.exit(1)

    with closing(output):
        try:
            for data in stream.iter_content(chunk_size=DOWNLOAD_CHUNK_SIZE):
                try:
                    output.write(data)
                except OSError:
                    sys.exit(1)
        except OSError:
            sys.exit(1)
        finally:
            stream.close()
