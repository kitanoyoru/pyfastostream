from streamlink import Streamlink
from streamlink.stream.ffmpegmux import MuxedStream
from streamlink.stream.stream import Stream

from pyfastostream.base import (
    IBaseStreamLink,
    PREFER_VIDEO,
    PREFER_AUDIO,
    STREAM_SYNONYMS,
    STREAM_BEST_QUALITY,
)


class StreamLinkFasto(IBaseStreamLink):
    def __init__(self):
        self.session = Streamlink()

    def set_options(self, key, value):
        return self.session.set_option(key, value)

    def resolve_url(self, url: str, stream_name: str, prefer: int) -> Stream:
        streams = self.session.streams(url)
        stabled = []
        for key, stream in streams.items():
            if prefer == PREFER_AUDIO:
                if "audio" in key:
                    if isinstance(stream, MuxedStream):
                        stabled.append((key, stream.substreams[1]))
                    else:
                        stabled.append((key, stream))
            elif prefer == PREFER_VIDEO:
                if "audio" not in key:
                    if isinstance(stream, MuxedStream):
                        stabled.append((key, stream.substreams[0]))
                    else:
                        stabled.append((key, stream))
            else:
                if not isinstance(stream, MuxedStream) and "audio" not in key:
                    stabled.append((key, stream))

        if not stabled:
            raise KeyError(stream_name)

        for key, stream in stabled:
            if key == stream_name:
                return stream

        if stream_name in STREAM_SYNONYMS:
            if stream_name == STREAM_BEST_QUALITY:
                return stabled[-1][1]

            return stabled[0][1]

        raise KeyError(stream_name)
