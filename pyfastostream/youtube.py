from youtube_dl import YoutubeDL

from pyfastostream.base import (
    IBaseStreamLink,
    PREFER_VIDEO,
    PREFER_AUDIO,
    STREAM_SYNONYMS,
    STREAM_BEST_QUALITY,
)


class YouTube(IBaseStreamLink):
    def __init__(self):
        self.session = YoutubeDL({"quiet": True})

    def resolve_url(self, url: str, stream_name: str, prefer: int) -> str:
        info_dict = self.session.extract_info(url, download=False)
        stabled = []
        for fmt in info_dict["formats"]:
            if prefer == PREFER_AUDIO:
                if fmt["acodec"] != "none" and fmt["vcodec"] == "none":
                    stabled.append(fmt)
            elif prefer == PREFER_VIDEO:
                if fmt["acodec"] == "none" and fmt["vcodec"] != "none":
                    stabled.append(fmt)
            else:
                if fmt["acodec"] != "none" and fmt["vcodec"] != "none":
                    stabled.append(fmt)

        if not stabled:
            raise KeyError(stream_name)

        if stream_name in STREAM_SYNONYMS:
            if stream_name == STREAM_BEST_QUALITY:
                return stabled[-1]["url"]

            return stabled[0]["url"]

        for fmt in stabled:
            if fmt["format_note"] == stream_name:
                return fmt["url"]

        raise KeyError(stream_name)
